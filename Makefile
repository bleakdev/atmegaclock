TARGET_EXEC ?= firmware

BUILD_DIR ?= ./bin
SRC_DIRS ?= ./src

DEVICE     = atmega32

CLOCK      = 16000000L
# PROGRAMMER = -c stk500v2 -P avrdoper
PROGRAMMER = -c jtag1 -P /dev/ttyUSB0


CC = avr-gcc -pedantic -Wall -Wextra -Wstrict-prototypes -Os -mcall-prologues -DF_CPU=$(CLOCK) -mmcu=$(DEVICE) -ffunction-sections -fdata-sections -ggdb3
CXX = avr-g++ -pedantic -Wall -Wextra -Os -mcall-prologues -DF_CPU=$(CLOCK) -mmcu=$(DEVICE) -std=c++17 -ffunction-sections -fdata-sections -fno-threadsafe-statics -ggdb3
LDFLAGS = -Wl,--gc-sections

# CC = avr-gcc -pedantic -Wall -Wextra -Wstrict-prototypes -Os -mcall-prologues -DF_CPU=$(CLOCK) -mmcu=$(DEVICE) -ffunction-sections -fdata-sections
# CXX = avr-g++ -pedantic -Wall -Wextra -Os -mcall-prologues -DF_CPU=$(CLOCK) -mmcu=$(DEVICE) -std=c++17 -ffunction-sections -fdata-sections -fno-threadsafe-statics
# LDFLAGS = -Wl,--gc-sections
AVRDUDE = avrdude $(PROGRAMMER) -p $(DEVICE) -B 0.128

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP

$(BUILD_DIR)/$(TARGET_EXEC).hex: $(BUILD_DIR)/$(TARGET_EXEC).elf
	$(RM) $(BUILD_DIR)/$(TARGET_EXEC).hex
	avr-objcopy -j .text -j .data -O ihex $< $@

$(BUILD_DIR)/$(TARGET_EXEC).elf: $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS)

# assembly
$(BUILD_DIR)/%.s.o: %.s
	$(MKDIR_P) $(dir $@)
	$(AS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@


.PHONY: clean install

clean:
	$(RM) -r $(BUILD_DIR)

install: $(BUILD_DIR)/$(TARGET_EXEC).hex
	$(AVRDUDE) -U flash:w:$(BUILD_DIR)/$(TARGET_EXEC).hex:i

-include $(DEPS)

MKDIR_P ?= mkdir -p
