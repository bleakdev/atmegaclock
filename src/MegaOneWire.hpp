#pragma once

#include <inttypes.h>

extern uint8_t M1WReset(uint8_t pin);
extern void M1WWriteBit(uint8_t pin, uint8_t bit);
extern void M1WWriteByte(uint8_t pin, uint8_t byte);
extern uint8_t M1WReadByte(uint8_t pin);
extern uint8_t M1WReadBit(uint8_t pin);
