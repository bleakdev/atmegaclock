#include "Ports.hpp"

#include <avr/io.h>

#define SETPIN(PORT, PIN, MODE) PORT = MODE ? PORT | (1 << PIN) : (PORT & ~(1 << PIN));
#define SETPORTVAL(ID, PORT, PIN, VAL) \
    case ID:                           \
        SETPIN(PORT, PIN, VAL)         \
        break;

// #define GETPIN(PORT, PIN) (PORT & (1 << PIN));
#define GETPIN(PORT, PIN) ((PORT >> PIN) & 0x01);

#define GETPORTVAL(ID, PORT, PIN) \
    case ID:                      \
        return GETPIN(PORT, PIN) break;

void setPinMode(const uint8_t pin, const uint8_t value)
{
    switch (pin)
    {
        SETPORTVAL(1, DDRB, PB0, value)
        SETPORTVAL(2, DDRB, PB1, value)
        SETPORTVAL(3, DDRB, PB2, value)
        SETPORTVAL(4, DDRB, PB3, value)
        SETPORTVAL(5, DDRB, PB4, value)
        SETPORTVAL(6, DDRB, PB5, value)
        SETPORTVAL(7, DDRB, PB6, value)
        SETPORTVAL(8, DDRB, PB7, value)

        SETPORTVAL(14, DDRD, PD2, value)
        SETPORTVAL(15, DDRD, PD2, value)
        SETPORTVAL(16, DDRD, PD2, value)
        SETPORTVAL(17, DDRD, PD3, value)
        SETPORTVAL(18, DDRD, PD4, value)
        SETPORTVAL(19, DDRD, PD5, value)
        SETPORTVAL(20, DDRD, PD6, value)
        SETPORTVAL(21, DDRD, PD7, value)

        SETPORTVAL(22, DDRC, PC0, value)
        SETPORTVAL(23, DDRC, PC1, value)

        SETPORTVAL(24, DDRC, PC2, value)
        SETPORTVAL(25, DDRC, PC3, value)
        SETPORTVAL(26, DDRC, PC4, value)
        SETPORTVAL(27, DDRC, PC5, value)

        SETPORTVAL(28, DDRC, PC6, value)
        SETPORTVAL(29, DDRC, PC7, value)

        SETPORTVAL(33, DDRA, PA7, value)
        SETPORTVAL(34, DDRA, PA6, value)
        SETPORTVAL(35, DDRA, PA5, value)
        SETPORTVAL(36, DDRA, PA4, value)
        SETPORTVAL(37, DDRA, PA3, value)
        SETPORTVAL(38, DDRA, PA2, value)
        SETPORTVAL(39, DDRA, PA1, value)
        SETPORTVAL(40, DDRA, PA0, value)
    }
}

void setPinValue(const uint8_t pin, const uint8_t value)
{
    switch (pin)
    {
        SETPORTVAL(1, PORTB, PB0, value)
        SETPORTVAL(2, PORTB, PB1, value)
        SETPORTVAL(3, PORTB, PB2, value)
        SETPORTVAL(4, PORTB, PB3, value)
        SETPORTVAL(5, PORTB, PB4, value)
        SETPORTVAL(6, PORTB, PB5, value)
        SETPORTVAL(7, PORTB, PB6, value)
        SETPORTVAL(8, PORTB, PB7, value)

        SETPORTVAL(14, PORTD, PD0, value)
        SETPORTVAL(15, PORTD, PD1, value)
        SETPORTVAL(16, PORTD, PD2, value)
        SETPORTVAL(17, PORTD, PD3, value)
        SETPORTVAL(18, PORTD, PD4, value)
        SETPORTVAL(19, PORTD, PD5, value)
        SETPORTVAL(20, PORTD, PD6, value)
        SETPORTVAL(21, PORTD, PD7, value)

        SETPORTVAL(22, PORTC, PC0, value)
        SETPORTVAL(23, PORTC, PC1, value)

        SETPORTVAL(24, PORTC, PC2, value)
        SETPORTVAL(25, PORTC, PC3, value)
        SETPORTVAL(26, PORTC, PC4, value)
        SETPORTVAL(27, PORTC, PC5, value)

        SETPORTVAL(28, PORTC, PC6, value)
        SETPORTVAL(29, PORTC, PC7, value)

        SETPORTVAL(33, PORTA, PA7, value)
        SETPORTVAL(34, PORTA, PA6, value)
        SETPORTVAL(35, PORTA, PA5, value)
        SETPORTVAL(36, PORTA, PA4, value)
        SETPORTVAL(37, PORTA, PA3, value)
        SETPORTVAL(38, PORTA, PA2, value)
        SETPORTVAL(39, PORTA, PA1, value)
        SETPORTVAL(40, PORTA, PA0, value)
    }
}

uint8_t getPinValue(const uint8_t pin)
{
    switch (pin)
    {
        GETPORTVAL(1, PINB, PB0)
        GETPORTVAL(2, PINB, PB1)
        GETPORTVAL(3, PINB, PB2)
        GETPORTVAL(4, PINB, PB3)
        GETPORTVAL(5, PINB, PB4)
        GETPORTVAL(6, PINB, PB5)
        GETPORTVAL(7, PINB, PB6)
        GETPORTVAL(8, PINB, PB7)

        GETPORTVAL(14, PIND, PD0)
        GETPORTVAL(15, PIND, PD1)
        GETPORTVAL(16, PIND, PD2)
        GETPORTVAL(17, PIND, PD3)
        GETPORTVAL(18, PIND, PD4)
        GETPORTVAL(19, PIND, PD5)
        GETPORTVAL(20, PIND, PD6)
        GETPORTVAL(21, PIND, PD7)

        GETPORTVAL(22, PINC, PC0)
        GETPORTVAL(23, PINC, PC1)

        GETPORTVAL(24, PINC, PC2)
        GETPORTVAL(25, PINC, PC3)
        GETPORTVAL(26, PINC, PC4)
        GETPORTVAL(27, PINC, PC5)

        GETPORTVAL(28, PINC, PC6)
        GETPORTVAL(29, PINC, PC7)

        GETPORTVAL(33, PINA, PA7)
        GETPORTVAL(34, PINA, PA6)
        GETPORTVAL(35, PINA, PA5)
        GETPORTVAL(36, PINA, PA4)
        GETPORTVAL(37, PINA, PA3)
        GETPORTVAL(38, PINA, PA2)
        GETPORTVAL(39, PINA, PA1)
        GETPORTVAL(40, PINA, PA0)
    }

    return 0;
}
