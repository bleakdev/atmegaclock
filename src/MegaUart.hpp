#pragma once

#include <inttypes.h>

void MUartInit(uint32_t baudRate);
void MUartPrint(const char *data);
void MUartPrintf(const char *data, ...);
void MUartPrintChr(const char data);
uint8_t MUartScanf(const char *format, ...);
