#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>

#include "MegaUart.hpp"
#include "Ports.hpp"

#define LCD_BRIGHTNESS 0xFF
#include <3rd/Rtc/ThreeWire.h>
#include <3rd/Rtc/RtcDS1302.h>
#include <3rd/rc-switch/RCSwitch.h>

#include "3rd/rc-switch/RCSwitch.h"
#include "MegaDS18B20.hpp"
#include "MegaDisplay.hpp"
#include "MegaDisplayPorts.hpp"
#include "RxTx443.hpp"
#include "Timings.hpp"

// #define RECEIVER
#define TRANSMITTER

#ifdef RECEIVER
RCSwitch receiver443Hz;
ISR(INT0_vect) { receiver443Hz.handleInterrupt(); }

ThreeWire myWire(19, 18, 20);  // IO, SCLK, CE
RtcDS1302<ThreeWire> Rtc(myWire);
MegaDisplay<NoritakeSerialPort<3, 2, 1>> display(20, 2);
#endif

#ifdef TRANSMITTER
RCSwitch transmitter443Hz;
#endif

int main()
{
#ifdef RECEIVER
    MCUCR |= (1 << ISC00);
    MCUCR &= ~(1 << ISC01);
    GICR |= (1 << INT0);

    receiver443Hz.setProtocol(6);
    receiver443Hz.enableReceive();

    Rtc.Begin();

    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
    RtcDateTime now = Rtc.GetDateTime();
    if (now < compiled)
    {
        Rtc.SetDateTime(compiled);
    }
#endif

#ifdef TRANSMITTER
    i2c_init();
    _delay_ms(100);
    MegaDisplay<I2CDisplay<0x26>> i2CDisplay(16, 2);
    transmitter443Hz.setProtocol(6);
    transmitter443Hz.enableTransmit(4);
#endif

    sei();

    while (true)
    {
#ifdef RECEIVER
        if (receiver443Hz.available())
        {
            display.gotoXY(12, 1);
            unsigned long t = receiver443Hz.getReceivedValue();
            display.printf("Ext:%1f  ", *reinterpret_cast<float *>(&t));
        }
        RtcDateTime dt = Rtc.GetDateTime();
        display.gotoXY(7, 0);
        display.printf("%2u:%2u", dt.Hour(), dt.Minute());

        float extTemp = MDS18B20GetTemp(5);
        display.gotoXY(0, 1);
        display.printf("Int:%1f  ", extTemp);
#endif

#ifdef TRANSMITTER
        float temp = MDS18B20GetTemp(5);
        // float temp = 0;
        i2CDisplay.gotoXY(0, 0);
        i2CDisplay.printf("Temp: %f   ", temp);
        unsigned long tempAsLong = *reinterpret_cast<unsigned long *>(&temp);
        transmitter443Hz.send(tempAsLong, 32);
#endif
    }

    return 1;
}
