#pragma once

#include "inttypes.h"

void delay(uint32_t us);
void startCountingTime();
uint16_t getCountedTime();
