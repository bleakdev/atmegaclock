#include "TextLib.h"

void PrintfBuffer(IOutputBuffer *outBuffer, const char *format, va_list vl)
{
    char *fptr = (char *)format;
    while (fptr != 0 && *fptr != '\0')
    {
        if (*fptr == '%')
        {
            fptr++;
            uint8_t fillSize = 0;
            if (isDigit(*fptr))
            {
                fillSize = *fptr++ - 48;
            }

            switch (*fptr)
            {
            case 'd':
            {
                fptr++;
                char buffer[5];
                int16_t value = (int16_t)va_arg(vl, int);
                if (value < 0)
                {
                    outBuffer->putChar('-');
                    value = -value;
                }

                int counter = 0;
                char *ptr = buffer;
                if (value == 0)
                {
                    outBuffer->putChar('0');
                    fillSize--;
                }
                while (value != 0)
                {
                    buffer[counter++] = (value % 10) + 48;
                    value /= 10;
                    ptr++;
                }
                for (int8_t f = fillSize - counter; f > 0; --f)
                {
                    outBuffer->putChar('0');
                }
                while (counter--)
                {
                    outBuffer->putChar(*(--ptr));
                }
            }
            break;
            case 'u':
            {
                fptr++;
                char buffer[5];
                uint16_t value = (uint16_t)va_arg(vl, unsigned int);

                int counter = 0;
                char *ptr = buffer;
                if (value == 0)
                {
                    outBuffer->putChar('0');
                    fillSize--;
                }
                while (value != 0)
                {
                    buffer[counter++] = (value % 10) + 48;
                    value /= 10;
                    ptr++;
                }
                for (int8_t f = fillSize - counter; f > 0; --f)
                {
                    outBuffer->putChar('0');
                }
                while (counter--)
                {
                    outBuffer->putChar(*(--ptr));
                }
            }
            break;
            case 'b':
            {
                fptr++;

                uint8_t value = (uint8_t)va_arg(vl, int);

                for (uint8_t i = 8; i != 0; --i)
                {
                    if (value & (1 << (i - 1))) outBuffer->putChar('1');
                    else
                        outBuffer->putChar('0');
                }
            }
            break;
            case 'f':
            {
                fptr++;
                char buffer[5];
                float value = va_arg(vl, double);
                if (value < 0)
                {
                    outBuffer->putChar('-');
                    value = -value;
                }

                uint16_t intPart = (uint16_t)(value);
                value -= intPart;

                int counter = 0;
                char *ptr = buffer;
                if (intPart == 0)
                {
                    outBuffer->putChar('0');
                }
                while (intPart != 0)
                {
                    buffer[counter++] = (intPart % 10) + 48;
                    intPart /= 10;
                    ptr++;
                }
                while (counter--)
                {
                    outBuffer->putChar(*(--ptr));
                }

                if (value > 0)
                {
                    outBuffer->putChar('.');
                }

                value *= 10;
                counter = fillSize > 0? fillSize : 5;
                char *lastNonZero = nullptr;
                ptr = buffer;
                while (value && counter--)
                {
                    // outBuffer->putChar((uint8_t)value + 48);
                    *ptr = (uint8_t)value;
                    value = value - *ptr;
                    value *= 10;

                    if (!counter && (uint8_t)value >= 5)
                    {
                        (*ptr)++;
                    }

                    if (*ptr != 0) lastNonZero = ptr;

                    ptr++;
                }
                ptr = buffer;
                while (lastNonZero && ptr <= lastNonZero)
                {
                    outBuffer->putChar(*ptr++ + 48);
                }
            }
            break;
            case 'c':
                fptr++;
                outBuffer->putChar(va_arg(vl, int));
                break;
            case 's':
            {
                fptr++;
                char *strPtr = va_arg(vl, char *);
                while (*strPtr != '\0')
                {
                    outBuffer->putChar(*strPtr++);
                }
            }
            break;
            }
        }
        else
        {
            outBuffer->putChar(*fptr);
            if (*fptr == '\n') outBuffer->putChar('\r');
            fptr++;
        }
    }
}

uint8_t isDigit(const char c) { return (c >= 48 && c <= 57); }

uint8_t isSpace(const char c)
{
    return c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f' || c == '\r';
}

uint16_t upow10(uint8_t pos)
{
    if (pos > 0)
    {
        return upow10(pos - 1) * 10;
    }
    else
    {
        return 1;
    }
}

float fpow10(int8_t pos)
{
    if (pos > 0)
    {
        return fpow10(pos - 1) * 10;
    }
    else if (pos < 0)
    {
        return fpow10(pos + 1) * 0.1;
    }
    else
    {
        return 1;
    }
}

float strToF(const char *str, char **endptr)
{
    const char *start = str;
    float value = 0;
    int8_t sign = 1;

    if (str[0] == '-')
    {
        sign = -1;
        str++;
    }

    int8_t counter = 0;
    uint8_t startCounting = 0;
    while (isDigit(*str) || *str == '.')
    {
        if (startCounting)
        {
            counter--;
        }

        if (*str == '.')
        {
            startCounting = 1;
        }

        str++;
    }
    *endptr = (char *)str;

    while (str-- != start)
    {
        if (*str == '.') continue;
        value += (*str - 48) * (counter > 4 ? 0 : fpow10(counter));
        counter++;
    }

    return value * sign;
}

int strtoi(const char *str, char **endptr)
{
    const char *start = str;
    int16_t value = 0;
    int8_t sign = 1;

    if (str[0] == '-')
    {
        sign = -1;
        str++;
    }

    while (isDigit(*str))
    {
        str++;
    }
    *endptr = (char *)str;

    int counter = 0;
    while (--str != start)
    {
        value += (*str - 48) * (counter > 4 ? 0 : upow10(counter));
        counter++;
    }

    return value * sign;
}

int strtou(const char *str, char **endptr)
{
    const char *start = str;
    uint16_t value = 0;

    while (isDigit(*str))
    {
        str++;
    }
    *endptr = (char *)str;

    int counter = 0;
    while (str-- != start)
    {
        value += (*str - 48) * (counter > 4 ? 0 : upow10(counter));
        counter++;
    }

    return value;
}
