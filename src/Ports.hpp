#pragma once

#include <inttypes.h>

#define INPUT 0
#define OUTPUT 1
#define HIGH 1
#define LOW 0

void setPinMode(const uint8_t pin, const uint8_t value);
void setPinValue(const uint8_t pin, const uint8_t value);
uint8_t getPinValue(const uint8_t pin);

#define digitalWrite(p, v) setPinValue(p, v)
#define digitalRead(p) getPinValue(p)
#define pinMode(p, v) setPinMode(p, v)

