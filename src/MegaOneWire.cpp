#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#include "Ports.hpp"
#include "Timings.hpp"

uint8_t M1WReset(uint8_t pin)
{
    uint8_t i;

    setPinValue(pin, 0);
    setPinMode(pin, 1);
    delay(480);

    setPinMode(pin, 0);
    delay(60);

    i = getPinValue(pin);
    delay(420);

    // 0 = OK
    return i;
}

void M1WWriteBit(uint8_t pin, uint8_t bit)
{
    setPinValue(pin, 0);
    setPinMode(pin, OUTPUT);
    delay(1);

    if (bit) setPinMode(pin, INPUT);

    delay(60);
    setPinMode(pin, INPUT);
}

uint8_t M1WReadBit(uint8_t pin)
{
    uint8_t value = 0;

    setPinValue(pin, 0);
    setPinMode(pin, OUTPUT);
    delay(1);

    setPinMode(pin, INPUT);
    delay(15);

    value = getPinValue(pin);

    delay(45);

    return value;
}

void M1WWriteByte(uint8_t pin, uint8_t byte)
{
    for (uint8_t i = 8; i; --i)
    {
        M1WWriteBit(pin, byte & 1);
        byte >>= 1;
    }
}

uint8_t M1WReadByte(uint8_t pin)
{
    uint8_t byte = 0x00;
    for (uint8_t i = 8; i; --i)
    {
        byte |= (M1WReadBit(pin) << (8 - i));
    }
    return byte;
}
