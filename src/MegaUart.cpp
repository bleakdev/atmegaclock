#include "MegaUart.hpp"

#include <avr/interrupt.h>
#include <stdarg.h>

#include "TextLib.h"

#define OBUFFER_SIZE 10
#define IBUFFER_SIZE 10

typedef struct
{
    char data[OBUFFER_SIZE];
    uint8_t start = 0;
    uint8_t end = 0;
} OutputBuffer;
volatile OutputBuffer outputBuffer;

#ifdef UART_READ
typedef struct
{
    char data[IBUFFER_SIZE + 1];  // (+1 for \0)
    uint8_t index;
} InputBuffer;
volatile InputBuffer inputBuffer = {.index = 0};
#endif

class UartOutputBuffer : public IOutputBuffer
{
public:
    void putChar(char c)
    {
        uint8_t pos = (outputBuffer.end + 1) % OBUFFER_SIZE;

        // Buffer is full, wait for flush the buffer
        if (pos == outputBuffer.start)
        {
            UCSRB |= (1 << UDRE);
        }
        while (pos == outputBuffer.start)
        {
        }
        if (pos != outputBuffer.start)
        {
            outputBuffer.data[outputBuffer.end] = c;
            outputBuffer.end = pos;
        }
        UCSRB |= (1 << UDRE);
    }
};

UartOutputBuffer uartOutputBuffer;

void MUartInit(uint32_t baudRate)
{
    uint8_t baudPrescale = (F_CPU - ((baudRate)*8L)) / ((baudRate)*16UL);
    UBRRH = (baudPrescale >> 8);
    UBRRL = baudPrescale;
#ifdef UART_READ
    UCSRB = (1 << TXEN) | (1 << RXEN) | (1 << RXCIE) | (1 << UDRE);
#else
    UCSRB = (1 << TXEN) /*| (1 << RXEN) */ | (1 << UDRE);
#endif
    UCSRC = (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1);
    UCSRA = 0x00;
}

static void output_buffer_send()
{
    if (outputBuffer.end != outputBuffer.start)
    {
        UDR = outputBuffer.data[outputBuffer.start];
        outputBuffer.start = (outputBuffer.start + 1) % OBUFFER_SIZE;
    }
}

ISR(USART_UDRE_vect)
{
    output_buffer_send();
    if (outputBuffer.end == outputBuffer.start)
    {
        UCSRB &= ~(1 << UDRE);
    }
}

void MUartPrint(const char *data)
{
    const char *ptr = data;
    if (*ptr == '\0') return;

    while (*ptr != '\0')
    {
        uartOutputBuffer.putChar(*(ptr++));
        if (*ptr == '\n') uartOutputBuffer.putChar('\r');
    }
}

void MUartPrintChr(const char data) { uartOutputBuffer.putChar(data); }

#ifdef UART_READ
ISR(USART_RXC_vect)
{
    const char input = UDR;
    output_buffer_add(input);

    if (inputBuffer.index < IBUFFER_SIZE)
    {
        inputBuffer.data[inputBuffer.index++] = input;
    }
}
#endif

uint8_t MUartScanf(const char *format, ...)
{
#ifdef UART_READ
    while (inputBuffer.index == 0 ||
           (inputBuffer.index > 0 && inputBuffer.data[inputBuffer.index - 1] != '\r'))
        ;
    MUartPrintf("\n");

    const char *data = inputBuffer.data;
    inputBuffer.index = 0;
    char *conversionEndPtr;
    uint8_t matched = 0;

    va_list vl;
    va_start(vl, format);

    const char *dptr = data;
    const char *fptr = format;
    while (dptr != 0 && *dptr != '\0' && *dptr != '\n' && *dptr != '\r')
    {
        if (*fptr == '%')
        {
            fptr++;
            uint8_t isMatched = 1;
            int amount = 0;
            if (isDigit(*fptr))
            {
                amount = strtoi(fptr, &conversionEndPtr);
                fptr = conversionEndPtr;
            }
            switch (*fptr)
            {
            case 'd':
                *(int *)va_arg(vl, int *) = strtoi(dptr, &conversionEndPtr);
                dptr = conversionEndPtr;
                break;
            case 'u':
                *(int *)va_arg(vl, int *) = strtou(dptr, &conversionEndPtr);
                dptr = conversionEndPtr;
                break;
            case 'f':
                *(float *)va_arg(vl, float *) = strToF(dptr, &conversionEndPtr);
                dptr = conversionEndPtr;
                break;
            case 'c':
                *(char *)va_arg(vl, char *) = *dptr;
                dptr++;
                break;
            case 's':
            {
                char *stringPtr = (char *)va_arg(vl, char *);
                if (amount == 0)
                {
                    while (!isSpace(*dptr))
                    {
                        *stringPtr++ = *dptr++;
                    }
                }
                else
                {
                    for (int i = 0; i < amount && !isSpace(*dptr); ++i)
                    {
                        *stringPtr++ = *dptr++;
                    }
                }
                *stringPtr++ = '\0';
            }
            break;
            default:
                isMatched = 0;
            }
            if (isMatched)
            {
                matched++;
            }
            else
            {
                return matched;
            }
            fptr++;
        }
        else if (*dptr != *fptr)
        {
            return matched;
        }

        dptr++;
        fptr++;
    }

    va_end(vl);
    return matched;
#else
    return 0;
#endif
}

void MUartPrintf(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    PrintfBuffer(&uartOutputBuffer, format, args);
    va_end(args);
}
