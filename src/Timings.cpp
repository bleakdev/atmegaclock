#include <util/delay_basic.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "Timings.hpp"

void delay(uint32_t us)
{
    if (us <= 1) return;
    if (us <= 2) return;
    while (us >= 65535)
    {
        _delay_loop_2(65535);
        us -= 65535;
    }
    _delay_loop_2((us << 2) - 10);
}

void startCountingTime()
{
    TCNT1 = 0;
    TCCR1B |= (1 << CS11);
}

uint16_t getCountedTime()
{
    TCCR1B &= ~(1 << CS11);
    return (TCNT1 - 1) >> 1; // -1 for reduce overhead of invoking function and returning a value
}
