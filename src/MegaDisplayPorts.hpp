#include <util/delay.h>

#include "Ports.hpp"

extern "C"
{
#include "i2cmaster.h"
}

template <uint8_t SIO, uint8_t STB, uint8_t SCK>
class NoritakeSerialPort
{
public:
    void init()
    {
        setPinMode(SIO, OUTPUT);
        setPinMode(STB, OUTPUT);
        setPinMode(SCK, OUTPUT);

        setPinValue(SIO, 1);
        setPinValue(STB, 1);
        setPinValue(SCK, 1);

        _delay_us(110000);

        write(0x30, false);
        _delay_us(10000);

        write(0x30, false);
        _delay_us(110);

        write(0x30, false);
        _delay_us(110);

        write(0x38, false);
        _delay_us(60);
    }

    void write(uint8_t data, uint8_t rs)
    {
        uint8_t x = 0xf8 + 2 * rs, i = 0x80;

        setPinValue(STB, 0);
        for (; i; i >>= 1)
        {
            setPinValue(SCK, 0);
            if (x & i) setPinValue(SIO, 1);
            else
                setPinValue(SIO, 0);
            setPinValue(SCK, 1);
        }

        x = data;
        for (i = 0x80; i; i >>= 1)
        {
            setPinValue(SCK, 0);
            if (x & i) setPinValue(SIO, 1);
            else
                setPinValue(SIO, 0);
            setPinValue(SCK, 1);
        }
        setPinValue(STB, 1);
    }

    uint8_t read(uint8_t rs)
    {
        uint8_t i = 0x80, data = 0xfc + 2 * rs;

        setPinValue(STB, 0);
        for (; i; i >>= 1)
        {
            setPinValue(SCK, 0);
            if (data & i) setPinValue(SIO, 1);
            else
                setPinValue(SIO, 0);
            setPinValue(SCK, 1);
        }

        setPinMode(SIO, INPUT);

        for (i = 0; i < 8; i++)
        {
            setPinValue(SCK, 0);
            data <<= 1;
            setPinValue(SCK, 1);
            if (getPinValue(SIO)) data |= 1;
        }

        setPinValue(STB, 1);
        setPinMode(SIO, OUTPUT);
        return data;
    }

    void setBrightness(uint8_t b)
    {
        write(0x30, false);
        _delay_us(5);
        write(~b, true);
        _delay_us(5);
    }
};

template <uint8_t ADDR>
class I2CDisplay
{
public:
    void init()
    {
        setDataBit(LCD_LED, 1);
        send();

        setDataBit(LCD_D0, 1);
        setDataBit(LCD_D1, 1);
        send();
        triggerE();
        _delay_us(5000);

        triggerE();
        _delay_us(100);

        triggerE();
        _delay_us(100);

        setDataBit(LCD_D0, 0);
        triggerE();

        _delay_us(1000);

        write(0x28, 0);
    }

    void write(uint8_t data, uint8_t rs)
    {
        setDataBit(LCD_RS, rs);
        setDataBit(LCD_RW, 0);

        for (uint8_t i = 0; i < 4; ++i)
        {
            setDataBit(LCD_D3 - i, data & (0x80 >> i));
        }
        send();

        triggerE();

        for (uint8_t i = 0; i < 4; ++i)
        {
            setDataBit(LCD_D3 - i, data & (0x08 >> i));
        }

        triggerE();
    }
    uint8_t read(uint8_t rs)
    {
        uint8_t ret = 0x00;
        setDataBit(LCD_RS, rs);
        setDataBit(LCD_RW, 1);
        send();
        setDataBit(LCD_E, 1);
        send();
        _delay_us(10);

        uint8_t data = readData();

        if ((data & (1 << LCD_D3)) == 0) ret |= 0x10;
        if ((data & (1 << LCD_D2)) == 0) ret |= 0x20;
        if ((data & (1 << LCD_D1)) == 0) ret |= 0x40;
        if ((data & (1 << LCD_D0)) == 0) ret |= 0x80;

        setDataBit(LCD_E, 0);
        send();

        _delay_us(10);

        setDataBit(LCD_E, 1);
        send();

        _delay_us(10);

        data = readData();

        if ((data & (1 << LCD_D3)) == 0) ret |= 0x01;
        if ((data & (1 << LCD_D2)) == 0) ret |= 0x02;
        if ((data & (1 << LCD_D1)) == 0) ret |= 0x04;
        if ((data & (1 << LCD_D0)) == 0) ret |= 0x08;

        setDataBit(LCD_E, 0);
        send();
        _delay_us(10);

        return ret;
    }

    void setBrightness(uint8_t) {}

private:
    const uint8_t LCD_D0 = 4;
    const uint8_t LCD_D1 = 5;
    const uint8_t LCD_D2 = 6;
    const uint8_t LCD_D3 = 7;
    const uint8_t LCD_RS = 0;
    const uint8_t LCD_RW = 1;
    const uint8_t LCD_E = 2;
    const uint8_t LCD_LED = 3;

    uint8_t m_currentData = 0x00;

    void setDataBit(uint8_t d, uint8_t value)
    {
        if (value) m_currentData |= (1 << d);
        else
            m_currentData &= ~(1 << d);
    }

    void send()
    {
        i2c_start((ADDR << 1) | I2C_WRITE);
        i2c_write(m_currentData);
        i2c_stop();
    }

    uint8_t readData()
    {
        uint8_t data;
        i2c_start((ADDR << 1) | I2C_READ);
        data = ~i2c_readNak();
        i2c_stop();
        return data;
    }

    void triggerE()
    {
        setDataBit(LCD_E, 1);
        send();
        _delay_us(2);

        setDataBit(LCD_E, 0);
        send();
        _delay_us(2);
    }
};
