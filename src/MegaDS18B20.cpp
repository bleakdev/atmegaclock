#include "MegaDS18B20.hpp"

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "MegaOneWire.hpp"

#define SKIPROM 0xCC
#define CONVTEMP 0x44
#define READSCRATCHPAD 0xBE

float MDS18B20GetTemp(uint8_t pin)
{
    uint8_t tempLowByte, tempHighByte;

    cli();

    M1WReset(pin);
    M1WWriteByte(pin, SKIPROM);
    M1WWriteByte(pin, CONVTEMP);

    while (!M1WReadBit(pin))
    {
    }

    M1WReset(pin);
    M1WWriteByte(pin, SKIPROM);
    M1WWriteByte(pin, READSCRATCHPAD);

    tempLowByte = M1WReadByte(pin);
    tempHighByte = M1WReadByte(pin);

    sei();

    return ((tempHighByte << 8) + tempLowByte) * 0.0625;
}
