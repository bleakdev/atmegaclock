#pragma once

#include <inttypes.h>
#include <stdarg.h>

class IOutputBuffer
{
public:
    virtual void putChar(char c) = 0;
};

uint8_t isDigit(const char c);
uint8_t isSpace(const char c);
uint16_t upow10(uint8_t pos);
float fpow10(int8_t pos);
float strToF(const char *str, char **endptr);
int strtoi(const char *str, char **endptr);
int strtou(const char *str, char **endptr);
void PrintfBuffer(IOutputBuffer *outBuffer, const char *format, va_list vl);
