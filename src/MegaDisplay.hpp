#pragma once

#define LCD_DDRAM 7
#define LCD_LINE0 0x00
#define LCD_LINE1 0x40
#define LCD_LINE2 0x14
#define LCD_LINE3 0x54

#include <inttypes.h>
#include <util/delay.h>

#include "TextLib.h"

template <typename Port>
class MegaDisplay : public IOutputBuffer
{
public:
    MegaDisplay(uint8_t cols, uint8_t lines)
        : m_cols{cols}
        , m_lines{lines}
    {
        m_port.init();
        init();
    }

    void print(const char *str)
    {
        while (*str)
        {
            putChar(*str++);
        }
    }

    void printf(const char *format, ...)
    {
        va_list args;

        va_start(args, format);
        PrintfBuffer(this, format, args);
        va_end(args);
    }

    void putChar(char c)
    {
        if (c == '\n')
        {
            newLine();
        }
        else
        {
            m_port.write(c, 1);
            m_current_col++;
            if (m_current_col >= m_cols)
            {
                newLine();
            }
        }
    }

    void setBrightness(uint8_t b) { m_port.setBrightness(b); }

    void clear()
    {
        cmd(0x01, 0);
        m_current_col = 0;
        m_current_line = 0;
    }

    void home()
    {
        cmd(0x02, 0);
        m_current_col = 0;
        m_current_line = 0;
    }

    void gotoXY(uint8_t x, uint8_t y)
    {
        switch (y)
        {
        case 0:
            cmd((1 << LCD_DDRAM) + LCD_LINE0 + x, 0);
            m_current_line = 0;
            m_current_col = x;
            break;
        case 1:
            cmd((1 << LCD_DDRAM) + LCD_LINE1 + x, 0);
            m_current_line = 1;
            m_current_col = x;
            break;
        case 2:
            cmd((1 << LCD_DDRAM) + LCD_LINE2 + x, 0);
            m_current_line = 2;
            m_current_col = x;
            break;
        case 3:
            cmd((1 << LCD_DDRAM) + LCD_LINE3 + x, 0);
            m_current_line = 3;
            m_current_col = x;
            break;
        }
    }

private:
    Port m_port;
    uint8_t m_cols;
    uint8_t m_lines;
    uint8_t m_current_col = 0;
    uint8_t m_current_line = 0;

    uint8_t waitBusy()
    {
        uint8_t c = 0x00;

        while ((c = m_port.read(0)) & (0x80))
        {
        }
        _delay_us(4);

        return m_port.read(0);
    }

    void cmd(uint8_t data, uint8_t rs)
    {
        // waitBusy();
        _delay_us(54);
        m_port.write(data, rs);
    }

    void newLine()
    {
        if (m_current_line < m_lines - 1)
        {
            gotoXY(0, m_current_line + 1);
        }
        else
        {
            gotoXY(0, 0);
        }
    }

    void init()
    {
        cmd(0x08, false);
        _delay_us(60);

        cmd(0x01, false);
        _delay_us(4000);

        cmd(0x06, false);
        _delay_us(60);

        cmd(0x0C, false);
        // cmd(0x0C, false);
        _delay_us(60);
    }
};
