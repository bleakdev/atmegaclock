
#include <cassert>
#include <iostream>
#include <string>

#include "../src/TextLib.cpp"
#include "../src/TextLib.h"

class TestOutputBuffer : public IOutputBuffer
{
public:
    void clear() { printedData = ""; }
    void putChar(char c) { printedData += c; }
    std::string printedData;
};
TestOutputBuffer tob;

void TestPrintf(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    PrintfBuffer(&tob, format, args);
    va_end(args);
}

void printUnsignedTests()
{
    tob.clear();

    TestPrintf("%u", 123);

    assert(tob.printedData == "123");
}

void printUnsignedWithFillTests()
{
    // There is nothing to fill
    tob.clear();
    TestPrintf("%2u", 123);
    assert(tob.printedData == "123");

    // Fill 2 zeroes
    tob.clear();
    TestPrintf("%5u", 123);
    assert(tob.printedData == "00123");

    // Fill 3 zeroes when value is equal 0
    tob.clear();
    TestPrintf("%3u", 0);
    assert(tob.printedData == "000");

    // Fill 1 zero when value is equal 0
    tob.clear();
    TestPrintf("%1u", 0);
    assert(tob.printedData == "0");
}

void printIntTest()
{
    // There is nothing to fill
    tob.clear();
    TestPrintf("%2d", 123);
    assert(tob.printedData == "123");

    // Fill 2 zeroes
    tob.clear();
    TestPrintf("%5d", 123);
    assert(tob.printedData == "00123");

    // Fill 3 zeroes when value is equal 0
    tob.clear();
    TestPrintf("%3d", 0);
    assert(tob.printedData == "000");

    // Fill 1 zero when value is equal 0
    tob.clear();
    TestPrintf("%1d", 0);
    assert(tob.printedData == "0");


    //--- The same for negative numbers
    // There is nothing to fill
    tob.clear();
    TestPrintf("%2d", -123);
    assert(tob.printedData == "-123");

    // Fill 2 zeroes
    tob.clear();
    TestPrintf("%5d", -123);
    assert(tob.printedData == "-00123");

    // Fill 3 zeroes when value is equal 0
    tob.clear();
    TestPrintf("%3d", -0);
    assert(tob.printedData == "000");

    // Fill 1 zero when value is equal 0
    tob.clear();
    TestPrintf("%1d", -0);
    assert(tob.printedData == "0");
}

void printFloatTest()
{
    // Without fraction
    tob.clear();
    TestPrintf("%f", 25.00);
    assert(tob.printedData == "25");

    // With fraction
    tob.clear();
    TestPrintf("%f", 25.12);
    assert(tob.printedData == "25.12");

    // Zero after dot
    tob.clear();
    TestPrintf("%f", 25.01);
    assert(tob.printedData == "25.01");

    // Negative number
    tob.clear();
    TestPrintf("%f", -25.01);
    assert(tob.printedData == "-25.01");
}

void printFloatWithFixedTest()
{
    // One number after dot without rounding
    tob.clear();
    TestPrintf("%1f", 1.21);
    assert(tob.printedData == "1.2");

    // Two numbers after dot without rounding
    tob.clear();
    TestPrintf("%2f", 1.21);
    assert(tob.printedData == "1.21");

    // Two numbers after dot without rounding with zero after dot
    tob.clear();
    TestPrintf("%2f", 1.01);
    assert(tob.printedData == "1.01");

    // Two numbers after dot without rounding with zero after dot and negative number
    tob.clear();
    TestPrintf("%2f", -1.01);
    assert(tob.printedData == "-1.01");

    // More numbers than precision
    tob.clear();
    TestPrintf("%3f", 1.0123);
    assert(tob.printedData == "1.012");

    // More numbers than precision with rounding
    tob.clear();
    TestPrintf("%3f", 1.0125);
    assert(tob.printedData == "1.013");

    // More numbers than precision - should not round yet
    tob.clear();
    TestPrintf("%3f", 1.01249);
    assert(tob.printedData == "1.012");
}

int main()
{
    printUnsignedTests();
    printUnsignedWithFillTests();

    printIntTest();

    printFloatTest();
    printFloatWithFixedTest();

    return 0;
}
